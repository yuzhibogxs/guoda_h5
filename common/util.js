import config from './config.js'

export default {
	async formatRichText(html){
		console.log(html)
		let text = await formatText(html)
		
		return parse(text)
	},
	formatText(html) {
		let newContent = html && html.replace(/<img[^>]*>/gi, function(match, capture) {
			match = match.replace(/style="[^"]+"/gi, '').replace(/style='[^']+'/gi, '');
			match = match.replace(/width="[^"]+"/gi, '').replace(/width='[^']+'/gi, '');
			match = match.replace(/height="[^"]+"/gi, '').replace(/height='[^']+'/gi, '');
			return match;
		});
		newContent = newContent.replace(/style="[^"]+"/gi, function(match, capture) {
			match = match.replace(/width:[^;]+;/gi, 'max-width:100%;').replace(/width:[^;]+;/gi, 'max-width:100%;');
			return match;
		});
		newContent = newContent.replace(/<br[^>]*\/>/gi, '');
		newContent = newContent.replace(/\<img/gi,
			'< img style="max-width:100%;height:auto;display:block;margin-top:0;margin-bottom:0;"');
		return newContent;
	},
	/**
	 * 判断手机是否ios
	*/
	isIos(){
		let platform = uni.getSystemInfoSync().platform
		if(platform=='android'){
			return false
		}
		return true
	},
	toast(msg){
		uni.showToast({
			title:msg,
			icon:'none'
		})
	},
	/**
	 * 富文本转换统一格式*
	*/
	 formatRichText(html){
	  let newContent= html.replace(/<img[^>]*>/gi,function(match,capture){
	    match = match.replace(/style="[^"]+"/gi, '').replace(/style='[^']+'/gi, '');
	    match = match.replace(/width="[^"]+"/gi, '').replace(/width='[^']+'/gi, '');
	    match = match.replace(/height="[^"]+"/gi, '').replace(/height='[^']+'/gi, '');
	    return match;
	  });
	  newContent = newContent.replace(/style="[^"]+"/gi,function(match,capture){
	    match = match.replace(/width:[^;]+;/gi, 'max-width:100%;').replace(/width:[^;]+;/gi, 'max-width:100%;');
	    return match;
	  });
	  newContent = newContent.replace(/<br[^>]*\/>/gi, '');
	  newContent = newContent.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin-top:0;margin-bottom:0;"');
	  return newContent;
	},
	showErr(msg){
		uni.showToast({
			title:msg,
			icon:'none'
		})
	},
	isLogin(){
		return new Promise((resolve,reject)=>{
			if(uni.getStorageSync('token')){
				resolve()
			}else{
				uni.showModal({
					title:'您还没有登录，是否去登录',
					success:res=>{
						if(res.confirm){
							uni.navigateTo({
								url:'/pages/login/index'
							})
						}
					}
				})
			} 

		})
	},
	/**
	 * 判断手机是否为iphoneX系列
	 */
	isIPhoneX() {
		let isIphoneX = false;
		let systemInfo = uni.getSystemInfoSync();
		// #ifdef MP
		if (systemInfo.model.search('iPhone X') != -1) {
			isIphoneX = true;
		}
		// #endif

		// #ifdef H5
		var u = navigator.userAgent;
		var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
		if (isIOS) {
			if (systemInfo.screenWidth == 375 && systemInfo.screenHeight == 812 && systemInfo.pixelRatio == 3) {
				isIphoneX = true;
			} else if (systemInfo.screenWidth == 414 && systemInfo.screenHeight == 896 && systemInfo.pixelRatio == 3) {
				isIphoneX = true;
			} else if (systemInfo.screenWidth == 414 && systemInfo.screenHeight == 896 && systemInfo.pixelRatio == 2) {
				isIphoneX = true;
			}
		}
		// #endif
		return isIphoneX;
	},

	/**
	 * 获取位置信息
	 */
	// getVillageName(message) {
	// 	var QQMapWX = require('./qqmap-wx-jssdk')
	// 	var qqmapsdk = new QQMapWX({
	// 		key: '23PBZ-LJWWX-QKJ4I-7SNVP-YQTLT-IXF5Y'
	// 	})
	// 	uni.getLocation({
	// 		type: 'wgs84',
	// 		success: (res) => {
	// 			qqmapsdk.reverseGeocoder({
	// 				location: {
	// 					// latitude:32.5425,
	// 					// longitude:114.04985,
	// 					latitude: res.latitude,
	// 					longitude: res.longitude
	// 				},
	// 				success: res => {
	// 					let obj = {
	// 						city: res.result.address_reference.landmark_l2.title, //村名
	// 						five: res.result.address + res.result.address_reference.town.title + res.result.address_reference.landmark_l2
	// 							.title //五级 城市 名
	// 					}
	// 					message(obj)
	// 				}
	// 			})
	// 		}
	// 	})
	// },
	 getDatetime() {
	    var d = new Date();
	    var year = d.getFullYear();
	    var month = change(d.getMonth() + 1);
	    var day = change(d.getDate());
	    var hour = change(d.getHours());
	    var minute = change(d.getMinutes());
	    var second = change(d.getSeconds());
	    
	    function change(t) {
	        if (t < 10) {
	            return "0" + t;
	        } else {
	            return t;
	        }
	    }
	
	    // var time = year + '-' + month + '-' + day + ' ' 
	    //         + hour + ':' + minute + ':' + second;
		var time=year+'-'+month+'-'+day
	
	    return time;
	},

	/**
	 * 时间戳转日期格式
	 * @param {Object} timeStamp
	 */
	timeStampTurnTime(timeStamp) {
		if (timeStamp != undefined && timeStamp != "" && timeStamp > 0) {
			var date = new Date();
			date.setTime(timeStamp * 1000);
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			m = m < 10 ? ('0' + m) : m;
			var d = date.getDate();
			d = d < 10 ? ('0' + d) : d;
			var h = date.getHours();
			h = h < 10 ? ('0' + h) : h;
			var minute = date.getMinutes();
			var second = date.getSeconds();
			minute = minute < 10 ? ('0' + minute) : minute;
			second = second < 10 ? ('0' + second) : second;
			return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
		} else {
			return "";
		}
	},

	/**
	 * 是否是微信浏览器
	 */
	isWeiXin() {
		var ua = navigator.userAgent.toLowerCase();
		if (ua.match(/MicroMessenger/i) == "micromessenger") {
			return true;
		} else {
			return false;
		}
	},

	/**
	 *	获取7天内的时间
	 */
	getDay(day) {
		var today = new Date();
		var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;
		today.setTime(targetday_milliseconds); //注意，这行是关键代码
		var tYear = today.getFullYear();
		var tMonth = today.getMonth();
		var tDate = today.getDate();
		var day = today.getDay()
		tMonth = this.doHandleMonth(tMonth + 1);
		tDate = this.doHandleMonth(tDate);
		day = this.doHandleDay(day)
		let timeStr = tYear + "-" + tMonth + "-" + tDate;
		let obj = {
			day,
			timeStr,
			tDate
		}
		return obj
	},
	doHandleMonth(month) {
		var m = month;
		if (month.toString().length == 1) {
			m = "0" + month;
		}
		return m;
	},
	doHandleDay(day) {
		let days
		switch (day) {
			case 1:
				days = '周一'
				break
			case 2:
				days = '周二'
				break
			case 3:
				days = '周三'
				break
			case 4:
				days = '周四'
				break
			case 5:
				days = '周五'
				break
			case 6:
				days = '周六'
				break
			case 0:
				days = '周日'
				break
		}
		return days
	},

	/**
	 * 图片路径转换
	 * @param {Object} img_path
	 */
	img(img_path) {
		var path = "";
		if (img_path != undefined && img_path != "") {
			if (img_path.indexOf("http://") == -1 && img_path.indexOf("https://") == -1) {
				path = config.resources  + img_path;
			} else {
				path = img_path;
			}
		}
		return path;
	},

	/**
	 * 根据经纬度计算两点间的距离 
	 * 
	 */

	getDistance(lat1, lng1, lat2, lng2) {
		let radLat1 = lat1 * Math.PI / 180.0
		let radLat2 = lat2 * Math.PI / 180.0
		let a = radLat1 - radLat2
		let b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0
		let s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
			Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)))
		s = s * 6378.137 // EARTH_RADIUS;-
		s = Math.round(s * 10000) / 10000
		return s
	},

	/**
	 * 生成uuid
	 * 
	 */
	uuid() {
		let mydate = new Date()
		let uuid = mydate.getDay() + mydate.getHours() + mydate.getMinutes() + mydate.getSeconds() + mydate.getMilliseconds() +
			Math.round(Math.random() * 10000);
		let num = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		return (uuid + "_" + num + "_" + num + "_" + num)
	},

	/**
	 * 距离换算
	 * 
	 */
	distance(num) {
		if (num >= 1000) return (num / 1000).toFixed(1) + 'km'
		else return num + 'm'
	},

	/**
	 * 公众号授权登录
	 * 
	 */
	login() {
		let code = ''
		let origin = 'http://hospital.chenhujia.com/user/#/pages/personal/transit'; //网页授权的回调域名，这里设置的是本地端口
		let urlNow = encodeURIComponent(origin); //处理域名
		let scope = 'snsapi_userinfo'; //弹框显示授权
		let appid = 'wx5ae22e0a6c13092e';
		let url =
			`https://open.weixin.qq.com/connect/oauth2/authorize?
					appid=${appid}&redirect_uri=${urlNow}&
					response_type=code&scope=${scope}&
					state=123#wechat_redirect`;
		window.location.href = url;
	},

	/**
	 * IM的处理时间函数
	 * 
	 */
	formatTime(timespan) {
		// timespan = timespan.replace(/-/g,"/")
		let dateTime = new Date(parseInt(timespan)) // 将传进来的字符串或者毫秒转为标准时间
		let year = dateTime.getFullYear()
		let month = dateTime.getMonth() + 1 < 10 ? '0' + (dateTime.getMonth() + 1) : dateTime.getMonth() + 1

		let day = dateTime.getDate() < 10 ? '0' + dateTime.getDate() : dateTime.getDate()
		let hour = dateTime.getHours() < 10 ? '0' + dateTime.getHours() : dateTime.getHours()
		let minute = dateTime.getMinutes() < 10 ? '0' + dateTime.getMinutes() : dateTime.getMinutes()

		// var second = dateTime.getSeconds()
		let millisecond = dateTime.getTime() // 将当前编辑的时间转换为毫秒
		let zero = new Date(new Date().setHours(0, 0, 0, 0));
		let yesterday = new Date((new Date().getTime()) - 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0)
		const d = new Date();
		const d1 = new Date(d.setFullYear(d.getFullYear() - 1));
		let t = new Date(new Date(d1.setDate(1)).setHours(0, 0, 0, 0));
		let t1, t2 = 0;
		t1 = t.setMonth(0);
		const d2 = new Date(t1);
		t2 = new Date(d2.setFullYear(d2.getFullYear() + 1)).setMilliseconds(-1);
		let now = new Date() // 获取本机当前的时间
		let nowNew = now.getTime() // 将本机的时间转换为毫秒
		let milliseconds = 0
		let timeSpanStr
		milliseconds = nowNew - millisecond
		// if (milliseconds <= 1000 * 60 * 1) { // 小于一分钟展示为刚刚
		// 	timeSpanStr = '刚刚'
		// } else 
		if (milliseconds <= 1000 * 60 * 60) { // 大于一分钟小于一小时展示为分钟
			timeSpanStr = Math.round((milliseconds / (1000 * 60))) + '分钟前'
		} else if (1000 * 60 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24) { // 大于一小时小于一天展示为小时
			// timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60)) + '小时前'
			if (millisecond < zero) {
				timeSpanStr = '昨天 ' + hour + ":" + minute
			} else {
				timeSpanStr = '今天 ' + hour + ":" + minute
			}

		} else if (milliseconds > 1000 * 60 * 60 * 24 * 15 && year === now.getFullYear()) {
			console.log('zuotian ')
			if (millisecond > yesterday) {
				timeSpanStr = '昨天 ' + hour + ":" + minute
			} else {
				timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute
			}
		} else {
			if (millisecond > yesterday) {
				timeSpanStr = '昨天 ' + hour + ":" + minute
			} else if (millisecond > t2) {
				timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute
			} else {
				timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute
			}

		}
		return timeSpanStr
	},
	/**
	 * 时间+0
	 * 
	 */
	timeStampTurn(timeStamp){
	if (timeStamp != undefined && timeStamp != "" && timeStamp > 0) {
		var date = new Date();
		date.setTime(timeStamp * 1000);
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		m = m < 10 ? ('0' + m) : m;
		var d = date.getDate();
		d = d < 10 ? ('0' + d) : d;
		var h = date.getHours();
		h = h < 10 ? ('0' + h) : h;
		var minute = date.getMinutes();
		var second = date.getSeconds();
		minute = minute < 10 ? ('0' + minute) : minute;
		second = second < 10 ? ('0' + second) : second;
		return h+':'+minute
	} else {
		return "";
	}	
	},
	/**
	 * 函数防抖
	 * 
	 */
	debounce(fn, delay) {
		var delay = delay || 200;
		var timer;
		return function() {
			var th = this;
			var args = arguments;
			if (timer) {
				clearTimeout(timer);
			}
			timer = setTimeout(function() {
				timer = null;
				fn.apply(th, args);
			}, delay);
		};
	},
	/**
	 * 获取await错误回调
	 * 
	 */
	awaitWrap(promise) {
		return promise
			.then(data => [null, {data}])
			.catch(err => [err, null])
	},
	formatMsgTime (timespan) {
		let time = timespan.replace(/-/g,"/")
		console.log(time)
	  let dateTime = new Date(time) // 将传进来的字符串或者毫秒转为标准时间
	 console.log(dateTime)
	  let year = dateTime.getFullYear()
	  let month = dateTime.getMonth() + 1
	  let day = dateTime.getDate()
	  let hour = dateTime.getHours()
	  let minute = dateTime.getMinutes()
	  var second = dateTime.getSeconds()
	  let millisecond = dateTime.getTime() // 将当前编辑的时间转换为毫秒
	  let now = new Date() // 获取本机当前的时间
	  let nowNew = now.getTime() // 将本机的时间转换为毫秒
	  let milliseconds = 0
	  let timeSpanStr
	  milliseconds = nowNew - millisecond
	  if (milliseconds <= 1000 * 60 * 1) { // 小于一分钟展示为刚刚
	    timeSpanStr = '刚刚'
	  } else if (1000 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60) { // 大于一分钟小于一小时展示为分钟
	    timeSpanStr = Math.round((milliseconds / (1000 * 60))) + '分钟前'
	  } else if (1000 * 60 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24) { // 大于一小时小于一天展示为小时
	    timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60)) + '小时前'
	  } else if (1000 * 60 * 60 * 24 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24 * 15) { // 大于一天小于十五天展示位天
	    timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60 * 24)) + '天前'
	  } else if (milliseconds > 1000 * 60 * 60 * 24 * 15 && year === now.getFullYear()) {
	    timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute
	  } else {
	    timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute
	  }
	  return timeSpanStr
	},
	
	/*
		解决异步上传
	*/
	async CustomForeach(arr, callback){
	  const length = arr.length;
	  const O = Object(arr);
	  let k = 0;
	  while (k < length) {
	    if (k in O) {
	      const kValue = O[k];
	      await callback(kValue, k, O);
	    }
	    k++;
	  }
	},
	/**
	 * 页面间传值
	 */
	navigator(path,data,methods){
		let params = '?'
		for (let i in data) {
			params=params+i+'='+data[i]+'&'
		}
		params = params.substring(0,params.length-1)
		let url = path+params
		console.log(url);
		let path1 = getCurrentPages()
		let pathMsg = path1[path1.length-1]
		if(pathMsg.$page.fullPath==url)return
		methods=methods?methods.toLowerCase():''
		if(methods=='redirect'){
			uni.redirectTo({
				url:url
			})
		}else if(methods=='relaunch'){
			uni.reLaunch({
				url:url
			})
		}else if(methods=='switchtab'){
			uni.switchTab({
				url:url
			})
		}else{
			uni.navigateTo({
				url:url
			})
		}
	},
	callPhone(phone){
		uni.makePhoneCall({
			phoneNumber:phone
		})
	},
	toWriteOff() {
		uni.scanCode({
			onlyFromCamera: true,
			scanType: ['qrCode'],
			success: (e) => {
				let data
				try {
					data = JSON.parse(e.result);
				} catch (e) {
					data = ''
				}
				console.log(data);
				if (data.code && data.orderId) {
					uni.util.navigator('/pages/mine/writeoff', data)
				} else {
					uni.showToast({
						title: '核销码错误！',
						icon: 'none'
					})
				}
			},
			fail: (e) => {
				console.log(e);
			}
		})
	},
	test(){
		console.log(123)
		return 123
	}
}
