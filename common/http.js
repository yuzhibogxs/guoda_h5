import config from './config.js'
// import store from '@/store/index.js'
export default {
	request(params= {}) {
		if (params.url) params.url = config.baseUrl + params.url
		let a= store.state.login.token
		let token
		// if(a) token = 'bearer' + ' ' + a
		return new Promise((resolve, reject) => {
			// let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsicmVzMSJdLCJleHAiOjE2MDUyNTU3NjEsInVzZXJfbmFtZSI6Im9HbThlNXpkdk9vRWZqek9VVjZmbjNmdXEzU0kiLCJqdGkiOiIyOTZmODMwMi0yYzQ1LTRlZmYtOWQxOS1kZDAxZWY4ODdiZWMiLCJjbGllbnRfaWQiOiJjbGllbnRJZCIsInNjb3BlIjpbImFsbCJdfQ.2DFEirfvLRlxBk_yOtsHPqN4fLb-dW3G6xrrN5Whk3w'
			uni.request({
				...params,
				header: {
					// 'content-type': 'application/x-www-form-urlencoded;application/json',
					"content-type": "application/json;charset=utf-8",
					"clientType":"user",
					"appId" :config.appid,
					'Authorization': token,
					// 'Authorization': 'bearer' +' '+'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsicmVzMSJdLCJleHAiOjE2MDU1NzgzNzksInVzZXJfbmFtZSI6Im9HbThlNXpkdk9vRWZqek9VVjZmbjNmdXEzU0kiLCJqdGkiOiIyNWJiYzQ3ZS1lZmYwLTRhM2MtYjQ1Zi03MmFmNjBiMzJlM2EiLCJjbGllbnRfaWQiOiJjbGllbnRJZCIsInNjb3BlIjpbImFsbCJdfQ.6i6WU71n10NHcTD1pCh3AzcUgYBTCKFrpkCN5YyllzA' ,

				},
				dataType: params.dataType || 'json',
				responseType: params.responseType || 'text',
				timeout: 12000,
				success: (res) => {
					if(res.statusCode == 200&&res.data.code==0) {
						// console.log('res:'+JSON.stringify(res)+':url'+params.url);
						resolve(res)
					}else{
						console.log(params.url) 
						console.log('err:'+JSON.stringify(res));
						reject(res);
					}
					if(res.statusCode==401){
						uni.removeStorageSync('token')
						uni.removeStorageSync('userInfo')
						uni.navigateTo({
							url: '/pages/login/index/index'
						})
						uni.showToast({
							title: '身份验证失效,请重新登录',
							icon: 'none'
						})
					}
					
					if(res.statusCode==500){
					}
					
					
				},
				fail: (res) => {
					console.log(params.url) 
					console.log(res)
					console.log('err:'+JSON.stringify(res));
					reject(res);
				},
				complete: (res) => {
					// reject(res);
				}
			})
		})
	}
}