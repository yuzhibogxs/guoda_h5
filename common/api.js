import http from '@/common/http'
export const REFUND_LIST = '';
//分类目录当前分类数据接口
// export function catalogCurrent(query) {
//   return request({
//     url: 'wx/catalog/current',
//     method: 'get',
//     params: query
//   }) 
// }
//获取验证码
export function sendSms(data) {
  return http.request({
    url: '/web/open/sms/send',
    method: 'post',
    data    
  })
  
}
//登录
export function login(data) {
  return http.request({
    url: '/web/open/app/login',
    method: 'get',
    data    
  })
}
//获取用户信息
export function getUser() {
  return http.request({
    url: '/webUser',
    method: 'get',
  })
}
//更新用户信息
export function setUser(data) {
  return http.request({
    url: '/webUser',
    method: 'PUT',
	data
  })
}
//常见问题列表
export function problemList() {
  return http.request({
    url: '/web/problem/list',
    method: 'get',
  })
}
//手机号更换
export function updatePhone(data) {
  return http.request({
    url: '/webUser/update/phone',
    method: 'get',
	data
  })
}
//
export function userOpinion(data) {
  return http.request({
    url: '/web/userOpinion',
    method: 'post',
	data
  })
}
//
export function orderInfo(data) {
  return http.request({
    url: '/orderInfo',
    method: 'post',
	data
  })
}
//取消订单
export function orderInfoCancel(id,data) {
  return http.request({
    url: '/orderInfo/cancel/'+id,
    method: 'post',
	data
  })
}
//
export function cloudPrescription(data) {
  return http.request({
    url: '/orderInfo/cloudPrescription',
    method: 'post',
	data
  })
}
//
export function cloudPrescriptionBack(data) {
  return http.request({
    url: '/orderInfo/cloudPrescriptionBack',
    method: 'post',
	data
  })
}
//
export function orderInfoList(data) {
  return http.request({
    url: '/orderInfo/list',
    method: 'get',
	data
  })
}
//订单退款
export function orderInfoRefund(data) {
  return http.request({
    url: '/orderInfo/refund/apply',
    method: 'post',
	data
  })
}
//查询订单状态
export function orderInfoStatus(data) {
  return http.request({
    url: '/orderInfo/status',
    method: 'get',
	data
  })
}
//核销
export function verification(code) {
  return http.request({
    url: '/orderInfo/verification?code='+code,
    method: 'post',
  })
}
//订单详情
export function orderInfoDetail(id,data) {
  return http.request({
    url: '/orderInfo/'+id,
    method: 'get',
	data
  })
}
//添加购物车
export function goodsAdd(data) {
  return http.request({
    url: "/web/shopcart",
    method: 'post',
	data
  })
}
//门店详情
export function storeDetail(id) {
  return http.request({
    url: "/web/open/pharmacy/"+id,
    method: 'get',
  })
}
//核销状态
export function verificationStatus(id) {
  return http.request({
    url: "/orderInfo/"+id+"/verificationStatus",
    method: 'get',
  })
}
//核销状态
export function orderInfoCheck(data) {
  return http.request({
    url: "/orderInfo/list/check",
    method: 'get',
	data
  })
}
//桃子医生回调
export function cloudPrescriptionCloudBack(id) {
  return http.request({
    url: "/web/cloudPrescription/cloudBack/"+id,
    method: 'get',
  })
}
//清空购物车
export function shopcartDelete(data) {
  return http.request({
    url: "/web/shopcart/delete",
    method: 'post',
	data
  })
}