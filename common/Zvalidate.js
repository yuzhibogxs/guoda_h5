const validate = {
	cycleValidate(value) {
		let arr = []
		for (let i = 0; i < value.length; i++) {
			if (value[i].id == "") {
				uni.showToast({
					title: `您添加的第${i+1}项职位信息有空缺，请添加信息或删除该职位`,
					icon: 'none',
					duration: 3000
				})
				arr.push(false)
				return false
			}
		}
		if (arr.length < 1) {
			return true
		} else {
			return false
		}
	},
	allcheck(value) {
		let arr = []
		for (let i = 0; i < value.length; i++) {
			if (value[i].option == "") {
				uni.showToast({
					title: `第${i+1}条选项为空，请添加选项或删除`,
					icon: 'none',
					duration: 3000
				})
				arr.push(false)
				return false
			}
		}
		if (arr.length < 1) {
			return true
		} else {
			return false
		}
	},
	idvalidate(value, method, msg) {
		if (value == '') {
			return true
		} else {
			if (!method.test(value)) {
				uni.showToast({
					title: msg,
					icon: 'none'
				})
				return false
			} else {
				return true
			}
		}
	},
	validateArr(arr,msg,method,warn){
		if(!arr.every(i=>{return i.name!=''})||!arr.every(i=>{return i.phone!=''})){
			uni.showToast({
				title:msg,
				icon:"none"
			})
			return false
		}else if(method){
			if(arr.every(i=>{return !method.test(i.phone)})){
				uni.showToast({
					title:warn,
					icon:"none"
				})
				return false
			}else{
				return true
			}
		}else{
			
			return true
		}
	},
	validateEmptyFor(emp,emp2, value, msg) {
		console.log(value)
		if (value == emp||value==emp2) {
			return true
		} else {
			let arr = value.filter(i => {
				return i.key == ''
			})
			let arr2 = value.filter(i => {
				return i.value == ''
			})
			if (arr.length > 0 || arr2.length > 0) {
				uni.showToast({
					title: msg,
					icon: 'none'
				})
				return false
			} else {
				return true
			}
		}
	},
	validateEmptyFn(emp, value, msg, method, warn) {
		if (value == emp) {
			return true
		} else {
			if (!value) {
				uni.showToast({
					title: msg,
					icon: 'none'
				})
				return false
			} else if (method) {
				if (!method.test(value)) {
					uni.showToast({
						title: warn,
						icon: 'none'
					})
					return false
				} else {
					return true
				}
			} else {
				return true
			}
		}
	},
	validateFn(value, msg, method, warn) {
		if (!value) {
			uni.showToast({
				title: msg,
				icon: 'none'
			})
			return false
		} else if (method) {
			if (!method.test(value)) {
				uni.showToast({
					title: warn,
					icon: 'none'
				})
				return false
			} else {
				return true
			}
		} else {
			return true
		}
	},
	reg(a, b, msg) {
		if (a != b) {
			uni.showToast({
				title: msg,
				icon: 'none'
			})
			return false
		} else {
			return true
		}
	},
	validate(arr) {
		return new Promise((reject, resolve) => {
			if (!arr.includes(false)) {
				reject()
			} else {
				return false
			}
		})
	},
}

export default validate
