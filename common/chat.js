import TIM from 'tim-wx-sdk';
import COS from "cos-wx-sdk-v5";
  
import http from './http.js'
import util from './util.js'
let options = {
	SDKAppID: 1400482900
 // 接入时需要将 0 替换为您的云通信应用的 SDKAppID
}
let tim = TIM.create(options);
tim.setLogLevel(1);
// tim.registerPlugin({
// 	'cos-js-sdk': COS
// });
tim.registerPlugin({'cos-wx-sdk': COS})
tim.on(TIM.EVENT.SDK_READY, function ready(res) {
	console.log('ready')
})

export default {
	tim,
	login() {
		return new Promise(async (resolve, reject) => {
			let {data} =await http.request({
				url:'im/getUserSig'
			})
			let obj = {
				userId: data.data.userId,
				userSig: data.data.userSig
			}
			
			tim.login({
				userID: obj.userId,
				userSig: obj.userSig
			}).then(res => {
				tim.on(TIM.EVENT.SDK_READY, function ready() {
					resolve(res)
				})
				tim.on(TIM.EVENT.ERROR, function(event) {
					// 收到 SDK 发生错误通知，可以获取错误码和错误信息
					// event.name - TIM.EVENT.ERROR
					// event.data.code - 错误码
					// event.data.message - 错误信息
					login()
				});

			}).catch(err => {
				console.log(err)
				reject(err)
			})
		})
	},
	listen() {
		return new Promise((resolve, reject) => {
			tim.on(TIM.EVENT.SDK_READY, function ready() {
				resolve()
			})
		})
	},
	logout() {

	},
	listenErr() {
		return new Promise((resolve, reject) => {
			tim.on(TIM.EVENT.ERROR, function(event) {
				// 收到 SDK 发生错误通知，可以获取错误码和错误信息
				// event.name - TIM.EVENT.ERROR
				// event.data.code - 错误码
				// event.data.message - 错误信息
				resolve(event)
			});
		})
	},
	//获取个人资料
	getMyProfile() {
		return new Promise((resolve, reject) => {
			tim.getMyProfile().then(function(imResponse) {
				resolve(imResponse)
				console.log(imResponse.data); // 个人资料 - Profile 实例
			}).catch(function(imError) {
				reject(imError)
				console.warn('getMyProfile error:', imError); // 获取个人资料失败的相关信息
			});
		})
	},
	//修改个人资料
	setMyProfile(nick, avatar) {
		return new Promise((resolve,reject)=>{
			tim.updateMyProfile({
				nick,
				avatar,
			}).then(function(imResponse) {
				resolve()
			}).catch(function(imError) {
				reject()
			});
		})
	},

	//获取对方信息
	getObjInfo(e) {
		let arr = []
		arr.push(e)
		console.log('对方信息：')
		console.log(e)
		return new Promise((resolve, reject) => {
			tim.getUserProfile({
				userIDList: arr
			}).then(res => {
				resolve(res)
			}).catch(err => {
				reject(err)
			})
		})
	},
	// 获取列表
	getConversationList() {
		return new Promise((resolve, reject) => {
			tim.getConversationList().then(function(imResponse) {
				resolve(imResponse.data.conversationList)
			}).catch(function(imError) {
				reject(imError)
				console.warn('getConversationList error:', imError); // 获取会话列表失败的相关信息
			});
		})
	},
	deleteConversation(id) {
		return new Promise((resolve, reject) => {
			tim.deleteConversation(id).then(imResponse => {
				resolve(imResponse)
			}).catch(err => {
				reject(err)
			})
		})
	},
	//获取message列表
	getMessageList(id) {
		let promise = tim.getMessageList({
			conversationID: 'C2C' + id,
			count: 15
		});
		return new Promise((resolve, reject) => {
			promise.then(function(imResponse) {
				resolve(imResponse)
			}).catch(function(err) {
				reject(err)
			})
		})
	},
	//设置消息已读
	setMessageRead(id){
		return tim.setMessageRead({conversationID:id})
	},
	//上拉获取旧消息
	getHistoryList(id, nextReqMessageID) {
		// 下拉查看更多消息
		return tim.getMessageList({
			conversationID: 'C2C' + id,
			nextReqMessageID,
			count: 15
		});
		// return new Promise((resolve, reject) => {
		// 	promise.then(function(imResponse) {
		// 		resolve(imResponse)

		// 	}).catch(err => {
		// 		reject(err)
		// 	})
		// })

	},
	// 开启消息列表监听
	listenMsg(fn) {
		tim.on(TIM.EVENT.MESSAGE_RECEIVED, (event) => {
			fn(event)
		});

	},
	//发送消息
	sendMessage(user, type, msg, sec) {
		let message
		if (type == 'text') {
			message = tim.createTextMessage({
				to: user,
				conversationType: TIM.TYPES.CONV_C2C,
				payload: {
					data: 'text',
					text: msg,
					extension: 'text'
				},
			});
		}else if(type=='image'){
			message = tim.createImageMessage({
			 to: user,
			  conversationType: TIM.TYPES.CONV_C2C,
			  payload: {
			    file: msg
			  },
			  onProgress: function(event) { console.log('file uploading:', event) }
			});
		}else if(type=='Audio'){
			   message = tim.createAudioMessage({
			   to: user,
			    conversationType: TIM.TYPES.CONV_C2C,
			    payload: {
			      file: msg
			    }
			  });		
		}else if (type == 'img') {
			//h5发送图片
			message = tim.createCustomMessage({
				to: user,
				conversationType: TIM.TYPES.CONV_C2C,
				payload: {
					data: 'image',
					description: msg,
					extension: ''
				}
			});
		} else if (type == 'audio') {
			//h5发送语音
			message = tim.createCustomMessage({
				to: user,
				conversationType: TIM.TYPES.CONV_C2C,
				payload: {
					data: 'audio',
					description: msg,
					extension: sec
				}
			});
		}
		return new Promise((resolve, reject) => {
			tim.sendMessage(message).then(function(imResponse) {
				resolve(imResponse)
				console.log("+_+_+_++_+_+_+_+_+_+_+")
				console.log(imResponse);
			}).catch(function(imError) {
				reject(imError)
				// alert(JSON.stringify(imError))
				console.warn('sendMessage error:', imError);
			});
		})

	},
	//监听语音
	// listenAudio(user) {
	// 	// 2.1 监听录音错误事件
	// 	recorderManager.onError(function(errMsg) {
	// 		console.warn('recorder error:', errMsg);
	// 	});
	// 	// 2.2 监听录音结束事件，录音结束后，调用 createAudioMessage 创建音频消息实例
	// 	recorderManager.onStop(function(res) {
	// 		console.log('recorder stop', res);
	// 		// 4. 创建消息实例，接口返回的实例可以上屏
	// 		const message = tim.createAudioMessage({
	// 			to: user,
	// 			conversationType: TIM.TYPES.CONV_C2C,
	// 			// 消息优先级，用于群聊（v2.4.2起支持）。如果某个群的消息超过了频率限制，后台会优先下发高优先级的消息，详细请参考：https://cloud.tencent.com/document/product/269/3663#.E6.B6.88.E6.81.AF.E4.BC.98.E5.85.88.E7.BA.A7.E4.B8.8E.E9.A2.91.E7.8E.87.E6.8E.A7.E5.88.B6)
	// 			// 支持的枚举值：TIM.TYPES.MSG_PRIORITY_HIGH, TIM.TYPES.MSG_PRIORITY_NORMAL（默认）, TIM.TYPES.MSG_PRIORITY_LOW, TIM.TYPES.MSG_PRIORITY_LOWEST
	// 			// priority: TIM.TYPES.MSG_PRIORITY_NORMAL,
	// 			payload: {
	// 				file: res
	// 			}
	// 		});

	// 		// 5. 发送消息
	// 		let promise = tim.sendMessage(message);
	// 		promise.then(function(imResponse) {
	// 			// 发送成功
	// 			console.log(imResponse);
	// 			console.log('发送成功')
	// 		}).catch(function(imError) {
	// 			// 发送失败
	// 			console.warn('sendMessage error:', imError);
	// 		});

	// 	});
	// },






}
