import {
	orderInfo,
	orderInfoStatus,
	orderInfoRefund,
	goodsAdd,
	verificationStatus
} from '@/common/api.js'

let throttle=false
let stop = false
const orderInfoStatus1 =(id,callback)=>{
	if(stop) return
	if (throttle) {
		setTimeout(() => {
			throttle = false
			orderInfoStatus1(id,callback)
		}, 1500)
	} else {
		throttle = true
		let data1 = {
			appId: '',
			orderId: id
		}
		orderInfoStatus(data1).then(success => {
			callback()
		}).catch(err => {
			console.log(throttle);
			orderInfoStatus1(id,callback)
		})
	}
}
const goodsAdds =(e)=>{
	new Promise((resolve,reject)=>{
		let item={
			pharmacyId:e.pharmacyId,
			goodsId:e.goodsId,
			num:e.goodsNum
		}
		goodsAdd(item).then(res=>{
			resolve(res)
		}).catch(err=>{
			reject(err)
		})
	})
} 
let throttle1=false
let stop1 = false
let doVerification=(id,callback)=>{
	if(stop1) return
	if (throttle1) {
		setTimeout(() => {
			throttle1 = false
			doVerification(id,callback)
		}, 1500)
	} else {
		throttle1 = true
		verificationStatus(id).then(success => {
			let isV = success.data.data
			console.log(isV=='true'||isV);
			if(isV=='true'||isV){
				console.log(callback);
				callback()
			}else{
				doVerification(id,callback)
			}
		}).catch(err => {
			uni.showToast({
				title:err.data.msg,
				icon:'none'
			})
		})
	}
}
const canGoodsAdd = true
export default {
	topay(payType, payInfo, id ,callback) {
		if (payType.toLowerCase() == 'alipay') {
			uni.requestPayment({
				provider: payType.toLowerCase(),
				orderInfo: payInfo,
				success: function(res) {
					console.log(res,'success');
					callback()
				},
				fail: function(res) {
					console.log(res,'fail');
					stop=true
					uni.showToast({
						title:res.errMsg,
						icon:'none'
					})
				},
			})
			orderInfoStatus1(id,callback)
		} else if (payType.toLowerCase() == 'wxpay') {
			//调起微信支付
		}
	},
	refund(data,callback){
		return new Promise((resolve,reject)=>{
			uni.showLoading({
				title:'退款申请提交中'
			})
			orderInfoRefund(data).then(data=>{
				uni.hideLoading()
				uni.showToast({
					title:'申请提交成功',
					icon:'success'
				})
				console.log(data);
				resolve(data)
				callback()
			}).catch(err=>{
				console.log(err);
				reject(err)
				uni.showToast({
					title:err.data.errMsg,
					icon:'none'
				})
			})
		})
	},
	againOrder(data){
		if(canGoodsAdd){
			canGoodsAdd=false
			let length = data.length
			let i = 0
			goodsAdds(data[i]).then(res=>{
				if(i<data.length){
					i++
					goodsAdds(data[i])
				}else{
					canGoodsAdd=true
					uni.redirectTo({
						url:'/pages/shopping/index'
					})
				}
			}).catch(err=>{
				canGoodsAdd=true
				uni.showToast({
					title:err.data.errMsg,
					icon:'none'
				})
			})
		}else{
			return false
		}
	},
	verification(id,callback){
		console.log('verification');
		stop1=false
		doVerification(id,callback)
	},
	stopVerification(){
		stop1=true
	}
}
