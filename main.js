import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
import http from './common/http'
import util from './common/util'
// <script type="text/javascript" src="https://js.cdn.aliyun.dcloud.net.cn/dev/uni-app/uni.webview.1.5.2.js"></script>
Vue.config.productionTip = false
Vue.use(uView);
uni.util=util
uni.http=http
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
